# Build
FROM golang:1.19-alpine as build-stage

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /code-practice-submission

# Run
FROM gcr.io/distroless/base-debian11 AS run-stage

WORKDIR /

COPY --from=build-stage /code-practice-submission /code-practice-submission

EXPOSE 8080

CMD ["/code-practice-submission"]
