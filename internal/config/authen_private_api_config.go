package config

import (
	"github.com/spf13/viper"
)

const configKey = "authen-private-api"

type AuthenPrivateAPIConfig struct {
	Clients map[string]string `mapstructure:"clients"`
}

func NewAuthenPrivateAPIConfig() (*AuthenPrivateAPIConfig, error) {
	config := &AuthenPrivateAPIConfig{}

	if !viper.InConfig(configKey) {
		return nil, nil
	}

	err := viper.UnmarshalKey(configKey, config)
	if err != nil {
		return nil, err
	}

	return config, nil
}
