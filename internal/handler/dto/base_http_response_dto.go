package dto

type BaseHttpResponseDTO struct {
	Data    any    `json:"data,omitempty"`
	Failure string `json:"failure,omitempty"`
}
