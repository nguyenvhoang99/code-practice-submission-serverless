package dto

type SubmitSubmissionHttpResponseDTO struct {
	Data *SubmitSubmissionData `json:"data,omitempty"`
}

type SubmitSubmissionData struct {
	Id string `json:"id,omitempty"`
}
