package dto

import (
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/service/model"
)

type GetSubmissionResultsHttpResponseDTO struct {
	Data          *GetSubmissionResultsData `json:"data,omitempty"`
	NextPageState string                    `json:"next_page_state,omitempty"`
}

type GetSubmissionResultsData struct {
	Results []*model.Result `json:"results"`
}

type UpdateSubmissionResultData struct {
	UserId     string `json:"user_id,omitempty"`
	QuestionId string `json:"question_id,omitempty"`
	*model.Result
}
