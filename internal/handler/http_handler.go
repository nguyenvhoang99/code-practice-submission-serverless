package handler

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/config"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/handler/dto"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/handler/interceptor"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/service"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/service/model"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/pkg/utils"
)

const (
	apiV1Prefix        = "/api/v1"
	apiV1PrivatePrefix = "/api/v1/private"
)

type HttpHandler struct {
	router                   *mux.Router
	authenInterceptor        interceptor.IHttpRequestInterceptor
	authenPrivateInterceptor interceptor.IHttpRequestInterceptor
	cipherService            service.ICipherService
	resultService            service.IResultService
	storageService           service.IStorageService
}

func NewHttpHandler(authenPrivateAPIConfig *config.AuthenPrivateAPIConfig,
	cipherService service.ICipherService,
	resultService service.IResultService,
	storageService service.IStorageService) http.Handler {
	handler := &HttpHandler{
		router:                   mux.NewRouter(),
		authenInterceptor:        interceptor.NewAuthenHttpRequestInterceptor(),
		authenPrivateInterceptor: interceptor.NewAuthenHttpPrivateRequestInterceptor(authenPrivateAPIConfig),
		cipherService:            cipherService,
		resultService:            resultService,
		storageService:           storageService,
	}

	handler.router.Methods(http.MethodPost).
		Path(apiV1Prefix + "/submissions").
		HandlerFunc(handler.authenInterceptor.Intercept(handler.submitSubmission))

	handler.router.Methods(http.MethodGet).
		Path(apiV1Prefix + "/submissions/results").
		HandlerFunc(handler.authenInterceptor.Intercept(handler.getSubmissionResults))

	handler.router.Methods(http.MethodPut).
		Path(apiV1PrivatePrefix + "/submissions/{submission_id}/results").
		HandlerFunc(handler.authenPrivateInterceptor.Intercept(handler.updateSubmissionResult))

	return handler.router
}

func (hh *HttpHandler) submitSubmission(reswr http.ResponseWriter, req *http.Request) {
	reswr.Header().Set("Content-Type", "application/json")

	userId := req.Context().
		Value(interceptor.AuthenHttpRequestContextKey{}).(interceptor.AuthenHttpRequestContextValue).
		UserId

	questionId := req.URL.Query().Get("question_id")
	if questionId == "" {
		reswr.WriteHeader(http.StatusBadRequest)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "missing query param question_id",
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	submission := &model.Submission{}

	reqBodyReader := json.NewDecoder(req.Body)
	err := reqBodyReader.Decode(submission)
	if err != nil {
		reswr.WriteHeader(http.StatusBadRequest)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "invalid body json: " + err.Error(),
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	submissionId, err := hh.storageService.Insert(req.Context(), userId, questionId, submission)
	if err != nil {
		reswr.WriteHeader(http.StatusInternalServerError)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "error when submit submission: " + err.Error(),
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	resBody := &dto.SubmitSubmissionHttpResponseDTO{
		Data: &dto.SubmitSubmissionData{
			Id: submissionId,
		},
	}
	resBodyWriter := json.NewEncoder(reswr)
	resBodyWriter.Encode(resBody)
}

func (hh *HttpHandler) getSubmissionResults(reswr http.ResponseWriter, req *http.Request) {
	reswr.Header().Set("Content-Type", "application/json")

	userId := req.Context().
		Value(interceptor.AuthenHttpRequestContextKey{}).(interceptor.AuthenHttpRequestContextValue).
		UserId

	questionId := req.URL.Query().Get("question_id")
	if questionId == "" {
		reswr.WriteHeader(http.StatusBadRequest)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "missing query param question_id",
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	var pageState []byte
	pageStateString := strings.ReplaceAll(req.URL.Query().Get("page_state"), " ", "")
	if pageStateString != "" {
		reqPageState, err := hh.cipherService.DecryptFromString(pageStateString)
		if err != nil {
			reswr.WriteHeader(http.StatusBadRequest)
			resBody := &dto.BaseHttpResponseDTO{
				Failure: "invalid query param page_state",
			}
			resBodyWriter := json.NewEncoder(reswr)
			resBodyWriter.Encode(resBody)
			return
		}
		pageState = reqPageState
	}

	var pageSize int
	pageSizeString := req.URL.Query().Get("page_size")
	if pageSizeString == "" {
		pageSizeString = "20"
	}
	pageSize, err := strconv.Atoi(pageSizeString)
	if err != nil || pageSize > 20 {
		reswr.WriteHeader(http.StatusBadRequest)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "invalid query param page_size",
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	results, nextPageState, err := hh.resultService.GetResults(req.Context(), userId, questionId, pageState, pageSize)
	if err != nil {
		reswr.WriteHeader(http.StatusInternalServerError)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "error when get submission results: " + err.Error(),
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}
	resNextPageState, err := hh.cipherService.EncryptToString(nextPageState)
	if err != nil {
		reswr.WriteHeader(http.StatusInternalServerError)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "error when encrypt nextPageState of submission results: " + err.Error(),
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	resBody := &dto.GetSubmissionResultsHttpResponseDTO{
		Data: &dto.GetSubmissionResultsData{
			Results: results,
		},
		NextPageState: resNextPageState,
	}
	resBodyWriter := json.NewEncoder(reswr)
	resBodyWriter.Encode(resBody)
}

func (hh *HttpHandler) updateSubmissionResult(reswr http.ResponseWriter, req *http.Request) {
	reswr.Header().Set("Content-Type", "application/json")

	reqVars := mux.Vars(req)
	if len(reqVars) == 0 ||
		reqVars["submission_id"] == "" {
		reswr.WriteHeader(http.StatusBadRequest)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "missing path param submission_id",
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	reqBody, err := utils.ClonetHttpRequestBody(req)
	if err != nil {
		reswr.WriteHeader(http.StatusInternalServerError)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "failed to read request body: " + err.Error(),
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	reqBodyReader := json.NewDecoder(reqBody)

	submissionResultData := &dto.UpdateSubmissionResultData{}
	err = reqBodyReader.Decode(&submissionResultData)
	if err != nil {
		reswr.WriteHeader(http.StatusBadRequest)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "invalid body json: " + err.Error(),
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	if submissionResultData.UserId == "" ||
		submissionResultData.QuestionId == "" ||
		submissionResultData.Details == nil {
		reswr.WriteHeader(http.StatusBadRequest)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "missing body key user_id or question_id or details",
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	submissionResultData.SubmissionId = reqVars["submission_id"]

	err = hh.resultService.UpdateResult(req.Context(), submissionResultData.UserId, submissionResultData.QuestionId, submissionResultData.Result)
	if err != nil {
		reswr.WriteHeader(http.StatusInternalServerError)
		resBody := &dto.BaseHttpResponseDTO{
			Failure: "error when update submission result: " + err.Error(),
		}
		resBodyWriter := json.NewEncoder(reswr)
		resBodyWriter.Encode(resBody)
		return
	}

	reswr.WriteHeader(http.StatusOK)
}
