package interceptor

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/config"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/handler/dto"
)

type AuthenHttpPrivateRequestInterceptor struct {
	Clients map[string]string
}

type AuthenHttpPrivateRequestContextKey struct {
}

type AuthenHttpPrivateRequestContextValue struct {
	ClientId string
}

func NewAuthenHttpPrivateRequestInterceptor(config *config.AuthenPrivateAPIConfig) IHttpRequestInterceptor {
	return &AuthenHttpPrivateRequestInterceptor{
		Clients: config.Clients,
	}
}

func (ahpri *AuthenHttpPrivateRequestInterceptor) Intercept(next http.HandlerFunc) http.HandlerFunc {
	return func(reswr http.ResponseWriter, req *http.Request) {
		reswr.Header().Set("Content-Type", "application/json")

		if req.Header == nil ||
			req.Header.Get("Client-Id") == "" ||
			req.Header.Get("Client-Key") == "" ||
			ahpri.Clients[req.Header.Get("Client-Id")] != req.Header.Get("Client-Key") {
			reswr.WriteHeader(http.StatusUnauthorized)
			resBody := &dto.BaseHttpResponseDTO{
				Failure: "invalid header Authorization",
			}
			resBodyWriter := json.NewEncoder(reswr)
			resBodyWriter.Encode(resBody)
			return
		}

		req = req.WithContext(context.WithValue(req.Context(),
			AuthenHttpPrivateRequestContextKey{},
			AuthenHttpPrivateRequestContextValue{
				ClientId: req.Header.Get("Client-Id"),
			}))

		next(reswr, req)
	}
}
