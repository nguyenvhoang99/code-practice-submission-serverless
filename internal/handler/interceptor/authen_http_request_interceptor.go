package interceptor

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/handler/dto"
)

type AuthenHttpRequestInterceptor struct {
}

type AuthenHttpRequestContextKey struct {
}

type AuthenHttpRequestContextValue struct {
	UserId string
}

func NewAuthenHttpRequestInterceptor() IHttpRequestInterceptor {
	return &AuthenHttpRequestInterceptor{}
}

func (ahri *AuthenHttpRequestInterceptor) Intercept(next http.HandlerFunc) http.HandlerFunc {
	return func(reswr http.ResponseWriter, req *http.Request) {
		reswr.Header().Set("Content-Type", "application/json")

		if req.Header == nil ||
			req.Header.Get("Authorization") == "" ||
			req.Header.Get("authorization") == "" {
			reswr.WriteHeader(http.StatusUnauthorized)
			resBody := &dto.BaseHttpResponseDTO{
				Failure: "missing header Authorization",
			}
			resBodyJson, _ := json.Marshal(resBody)
			reswr.Write(resBodyJson)
			return
		}

		authorization := req.Header.Get("Authorization")
		if authorization == "" {
			authorization = req.Header.Get("authorization")
		}
		_, userToken, ok := strings.Cut(authorization, "Bearer ")
		if !ok || userToken == "" {
			reswr.WriteHeader(http.StatusUnauthorized)
			resBody := &dto.BaseHttpResponseDTO{
				Failure: "invalid header Authorization",
			}
			resBodyJson, _ := json.Marshal(resBody)
			reswr.Write(resBodyJson)
			return
		}

		// TODO: Get userId from userToken
		userId := "53567cf6-ef40-425f-bb43-e6666124b3d0"

		req = req.WithContext(context.WithValue(req.Context(),
			AuthenHttpRequestContextKey{},
			AuthenHttpRequestContextValue{
				UserId: userId,
			}))

		next(reswr, req)
	}
}
