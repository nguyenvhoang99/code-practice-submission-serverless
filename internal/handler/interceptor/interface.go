package interceptor

import "net/http"

type IHttpRequestInterceptor interface {
	Intercept(next http.HandlerFunc) http.HandlerFunc
}
