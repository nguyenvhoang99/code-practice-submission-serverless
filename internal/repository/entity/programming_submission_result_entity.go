package entity

type ProgrammingSubmissionResultEntity struct {
	SubmissionId     string
	UserId           string
	QuestionId       string
	CompileStatus    int
	RunStatus        int
	CorrectTestcases int
	TotalTestcases   int
}
