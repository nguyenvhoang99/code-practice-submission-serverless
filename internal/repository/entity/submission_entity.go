package entity

type SubmissionEntity struct {
	SubmissionId      string
	UserId            string
	QuestionId        string
	Content           string
	Metadata          map[string]string
	CreatedInYYYYmmdd int32
	CreatedInYYYYmm   int32
	CreatedAt         int64
	UpdatedAt         int64
}
