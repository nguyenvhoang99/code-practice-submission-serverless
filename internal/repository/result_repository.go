package repository

import (
	"context"

	"github.com/gocql/gocql"

	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/repository/entity"
)

type IResultRepository interface {
	UpdateProgrammingResult(ctx context.Context, userId, questionId string, entity *entity.ProgrammingSubmissionResultEntity) error
	GetProgrammingResults(ctx context.Context, userId, questionId string, pageState []byte, pageSize int) ([]*entity.ProgrammingSubmissionResultEntity, []byte, error)
}

type ResultRepository struct {
	cassandraClient *gocql.Session
}

func NewResultRepository(cassandraClient *gocql.Session) IResultRepository {
	return &ResultRepository{
		cassandraClient: cassandraClient,
	}
}

func (rr *ResultRepository) UpdateProgrammingResult(ctx context.Context, userId, questionId string, entity *entity.ProgrammingSubmissionResultEntity) error {
	return rr.cassandraClient.Query(
		`
		UPDATE programming_submission_result
		SET compile_status = ?, run_status = ?, correct_testcases = ?, total_testcases = ?
		WHERE user_id = ? AND question_id = ? AND submission_id = ?
		`,
		entity.CompileStatus, entity.RunStatus, entity.CorrectTestcases, entity.TotalTestcases,
		userId, questionId, entity.SubmissionId,
	).WithContext(ctx).Exec()
}

func (rr *ResultRepository) GetProgrammingResults(ctx context.Context, userId, questionId string, pageState []byte, pageSize int) ([]*entity.ProgrammingSubmissionResultEntity, []byte, error) {
	entities := make([]*entity.ProgrammingSubmissionResultEntity, 0)

	var submissionId string
	var compileStatus, runStatus, correctTestcases, totalTestcases int

	results := rr.cassandraClient.Query(
		`
    SELECT submission_id, compile_status, run_status, correct_testcases, total_testcases
    FROM programming_submission_result
    WHERE user_id = ? AND question_id = ?
    `,
		userId, questionId,
	).WithContext(ctx).PageState(pageState).PageSize(pageSize).Iter()

	for results.Scan(&submissionId, &compileStatus, &runStatus, &correctTestcases, &totalTestcases) {
		entities = append(entities,
			&entity.ProgrammingSubmissionResultEntity{
				SubmissionId:     submissionId,
				CompileStatus:    compileStatus,
				RunStatus:        runStatus,
				CorrectTestcases: correctTestcases,
				TotalTestcases:   totalTestcases,
			})
	}
	if err := results.Close(); err != nil {
		return nil, nil, err
	}

	return entities, results.PageState(), nil
}
