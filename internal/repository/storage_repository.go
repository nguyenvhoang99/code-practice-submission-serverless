package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/gocql/gocql"

	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/constant"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/repository/entity"
)

type IStorageRepository interface {
	Insert(ctx context.Context, entity *entity.SubmissionEntity) error
	GetByCreatedInFormat(ctx context.Context, formatType, formatValue string, pageState []byte, pageSize int) ([]*entity.SubmissionEntity, []byte, error)
}

type StorageRepository struct {
	cassandraClient *gocql.Session
}

func NewStorageRepository(cassandraClient *gocql.Session) IStorageRepository {
	return &StorageRepository{
		cassandraClient: cassandraClient,
	}
}

func (sr *StorageRepository) Insert(ctx context.Context, entity *entity.SubmissionEntity) error {
	metadatJson, err := json.Marshal(entity.Metadata)
	if err != nil {
		log.Printf("failed to marshal metadata of submission: %v", err)
		return err
	}
	metadatJsonString := string(metadatJson)
	now := time.Now()

	return sr.cassandraClient.Query(
		`
		INSERT INTO "submission"
		("submission_id", "user_id", "question_id", "content", "metadata", "created_in_yyyymmdd", "created_in_yyyymm", "created_at")
		VALUES
		(?, ?, ?, ?, ?, ?, ?, ?)
		`,
		entity.SubmissionId, entity.UserId, entity.QuestionId, entity.Content, metadatJsonString,
		now.Format("20060102"), now.Format("200601"), now.UnixMilli(),
	).WithContext(ctx).Exec()
}

func (sr *StorageRepository) GetByCreatedInFormat(ctx context.Context, formatType, formatValue string, pageState []byte, pageSize int) ([]*entity.SubmissionEntity, []byte, error) {
	entities := make([]*entity.SubmissionEntity, 0)

	var submissionId, userId, questionId, content, metadatJsonString string
	var createdAt int64

	query := `
	SELECT "submission_id", "user_id", "question_id", "content", "metadata", "created_at"
	FROM "submission"
	WHERE 
	`
	switch strings.ToLower(formatType) {
	case constant.BY_YYYYMMDD:
		query += "created_in_yyyymmdd = ?"
	case constant.BY_YYYYMM:
		query += "created_in_yyyymm = ?"
	default:
		return nil, nil, fmt.Errorf("unimplemented formatType=%s", formatType)
	}
	results := sr.cassandraClient.Query(query, formatValue).
		WithContext(ctx).PageState(pageState).PageSize(pageSize).Iter()

	for results.Scan(&submissionId, &userId, &questionId, &content, &metadatJsonString, &createdAt) {
		metadata := make(map[string]string)
		err := json.Unmarshal([]byte(metadatJsonString), &metadata)
		if err != nil {
			log.Printf("failed to unmarshal metadata of submission and ignore: %v", err)
		}

		entities = append(entities,
			&entity.SubmissionEntity{
				SubmissionId: submissionId,
				UserId:       userId,
				QuestionId:   questionId,
				Content:      content,
				Metadata:     metadata,
				CreatedAt:    createdAt,
			})
	}
	if err := results.Close(); err != nil {
		return nil, nil, err
	}

	return entities, results.PageState(), nil
}
