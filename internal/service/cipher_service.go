package service

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
)

type ICipherService interface {
	EncryptToString(data []byte) (string, error)
	DecryptFromString(data string) ([]byte, error)
}

type CipherService struct {
	secretKey []byte
}

func NewCipherService() ICipherService {
	return &CipherService{
		secretKey: []byte("l/ZThfvOqfRvb/fQ+O7zk30L8pdy+1I="),
	}
}

// rawData >> encryptedData >> encodedData >> X >> decodedData >> encryptedData >> rawData

func (cs *CipherService) EncryptToString(rawData []byte) (string, error) {
	block, err := aes.NewCipher(cs.secretKey)
	if err != nil {
		return "", err
	}

	encryptedData := make([]byte, len(rawData))

	iv := make([]byte, aes.BlockSize)
	rand.Read(iv)

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(encryptedData, rawData)

	encodedData := base64.StdEncoding.EncodeToString(encryptedData)

	return encodedData, nil
}

func (cs *CipherService) DecryptFromString(data string) ([]byte, error) {
	decodedData, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return nil, err
	}

	block, err := aes.NewCipher(cs.secretKey)
	if err != nil {
		return nil, err
	}

	iv := decodedData[:aes.BlockSize]
	encryptedData := decodedData[aes.BlockSize:]

	rawData := make([]byte, len(encryptedData))

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(rawData, encryptedData)

	return rawData, nil
}
