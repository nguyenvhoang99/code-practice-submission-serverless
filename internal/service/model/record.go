package model

type Submission struct {
	SubmissionId string
	UserId       string
	QuestionId   string
	Content      string
	Metadata     map[string]string
	CreatedAt    int64
}
