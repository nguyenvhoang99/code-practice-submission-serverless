package model

type Result struct {
	SubmissionId string `json:"submission_id"`
	Details      any    `json:"details"`
}

type ProgrammingResult struct {
	CompileStatus    int `json:"compile_status"`
	RunStatus        int `json:"run_status"`
	CorrectTestcases int `json:"correct_testcases"`
	TotalTestcases   int `json:"total_testcases"`
}
