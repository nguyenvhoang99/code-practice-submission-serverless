package service

import (
	"context"
	"encoding/json"
	"log"

	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/repository"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/repository/entity"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/service/model"
)

type IResultService interface {
	UpdateResult(ctx context.Context, userId, questionId string, submissionResult *model.Result) error
	GetResults(ctx context.Context, userId, questionId string, pageState []byte, pageSize int) ([]*model.Result, []byte, error)
}

type ResultService struct {
	repository repository.IResultRepository
}

func NewResultService(repository repository.IResultRepository) IResultService {
	return &ResultService{
		repository: repository,
	}
}

func (rs *ResultService) UpdateResult(ctx context.Context, userId, questionId string, submissionResult *model.Result) error {
	questionType := "PROGRAMMING" // TODO: Query questionType from questionId
	switch questionType {
	default:
		submissionResultDetailsJson, err := json.Marshal(submissionResult.Details)
		if err != nil {
			log.Printf("failed to marshal data of submission result: %v", err)
			return err
		}

		programmingResultDetails := &model.ProgrammingResult{}

		err = json.Unmarshal(submissionResultDetailsJson, programmingResultDetails)
		if err != nil {
			log.Printf("failed to unmarshal data of programming submission result: %v", err)
			return err
		}

		entity := &entity.ProgrammingSubmissionResultEntity{
			SubmissionId:     submissionResult.SubmissionId,
			CompileStatus:    programmingResultDetails.CompileStatus,
			RunStatus:        programmingResultDetails.RunStatus,
			CorrectTestcases: programmingResultDetails.CorrectTestcases,
			TotalTestcases:   programmingResultDetails.TotalTestcases,
		}
		return rs.repository.UpdateProgrammingResult(ctx, userId, questionId, entity)
	}
}

func (rs *ResultService) GetResults(ctx context.Context, userId, questionId string, pageState []byte, pageSize int) ([]*model.Result, []byte, error) {
	questionType := "PROGRAMMING" // TODO: Query questionType from questionId
	switch questionType {
	default:
		entities, nextPageState, err := rs.repository.GetProgrammingResults(ctx, userId, questionId, pageState, pageSize)
		if err != nil {
			return nil, nil, err
		}

		results := make([]*model.Result, 0)
		for _, entity := range entities {
			result := &model.Result{
				SubmissionId: entity.SubmissionId,
				Details: &model.ProgrammingResult{
					CompileStatus:    entity.CompileStatus,
					RunStatus:        entity.RunStatus,
					CorrectTestcases: entity.CorrectTestcases,
					TotalTestcases:   entity.TotalTestcases,
				},
			}
			results = append(results, result)
		}
		return results, nextPageState, nil
	}
}
