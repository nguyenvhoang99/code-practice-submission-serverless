package service

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/constant"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/repository"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/repository/entity"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/service/model"
)

type IStorageService interface {
	Insert(ctx context.Context, userId, questionId string, submission *model.Submission) (string, error)
	GetInDate(ctx context.Context, yyyymmdd string, pageState []byte, pageSize int) ([]*model.Submission, []byte, error)
}

type StorageService struct {
	storageRepository repository.IStorageRepository
}

func NewStorageService(storageRepository repository.IStorageRepository) IStorageService {
	return &StorageService{
		storageRepository: storageRepository,
	}
}

func (ss *StorageService) Insert(ctx context.Context, userId, questionId string, submission *model.Submission) (string, error) {
	submissionId := uuid.New().String()

	entity := &entity.SubmissionEntity{
		SubmissionId: submissionId,
		UserId:       userId,
		QuestionId:   questionId,
		Content:      submission.Content,
		Metadata:     submission.Metadata,
	}
	err := ss.storageRepository.Insert(ctx, entity)
	if err != nil {
		return "", err
	}

	return submissionId, nil
}

func (ss *StorageService) GetInDate(ctx context.Context, yyyymmdd string, pageState []byte, pageSize int) ([]*model.Submission, []byte, error) {
	entities, nextPageState, err := ss.storageRepository.GetByCreatedInFormat(ctx, constant.BY_YYYYMMDD, yyyymmdd, pageState, pageSize)
	if err != nil {
		return nil, nil, err
	}

	records := make([]*model.Submission, 0)
	for _, entity := range entities {
		record := &model.Submission{
			SubmissionId: entity.SubmissionId,
			UserId:       entity.UserId,
			QuestionId:   entity.QuestionId,
			Content:      entity.Content,
			Metadata:     entity.Metadata,
			CreatedAt:    entity.CreatedAt,
		}
		records = append(records, record)
	}
	return records, nextPageState, nil
}
