package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	cassandra_database "gitlab.com/nguyenvhoang99/code-practice-submission-serverless/pkg/database/cassandra"
	cassandra_database_config "gitlab.com/nguyenvhoang99/code-practice-submission-serverless/pkg/database/cassandra/config"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/pkg/loader"
	http_server "gitlab.com/nguyenvhoang99/code-practice-submission-serverless/pkg/server/http"
	http_server_config "gitlab.com/nguyenvhoang99/code-practice-submission-serverless/pkg/server/http/config"

	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/config"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/handler"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/repository"
	"gitlab.com/nguyenvhoang99/code-practice-submission-serverless/internal/service"
)

func main() {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	err := loader.NewConfigLoader()
	if err != nil {
		log.Fatalf("failed to init config loader: %v", err)
	}

	cassandraConfig, err := cassandra_database_config.NewCassandraConfig()
	if err != nil {
		log.Fatalf("invalid cassandra database config: %v", err)
	}
	cassandraClient, err := cassandra_database.NewCassandraClient(cassandraConfig)
	if err != nil {
		log.Fatalf("failed to create cassandra database client: %v", err)
	}

	authenPrivateAPIConfig, err := config.NewAuthenPrivateAPIConfig()
	if err != nil {
		log.Fatalf("invalid authen private api config: %v", err)
	}

	cipherService := service.NewCipherService()

	storageRepository := repository.NewStorageRepository(cassandraClient)
	storageService := service.NewStorageService(storageRepository)

	resultRepository := repository.NewResultRepository(cassandraClient)
	resultService := service.NewResultService(resultRepository)

	httpHandler := handler.NewHttpHandler(authenPrivateAPIConfig, cipherService, resultService, storageService)

	httpConfig, err := http_server_config.NewHttpConfig()
	if err != nil {
		log.Fatalf("invalid http server config: %v", err)
	}
	httpServer, err := http_server.NewHttpServer(httpConfig, httpHandler)
	if err != nil {
		log.Fatalf("failed to create http server: %v", err)
	}

	<-stop

	if httpServer != nil {
		http_server.Stop(httpServer)
	}

	if cassandraClient != nil {
		cassandra_database.Disconnect(cassandraClient)
	}
}
