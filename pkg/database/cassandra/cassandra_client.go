package cassandra_database

import (
	"crypto/tls"
	"log"

	"github.com/gocql/gocql"

	config "gitlab.com/nguyenvhoang99/code-practice-submission-serverless/pkg/database/cassandra/config"
)

func NewCassandraClient(config *config.CassandraConfig) (*gocql.Session, error) {
	cluster := gocql.NewCluster(config.ContactPoints...)

	cluster.Port = config.Port

	if config.UseSSL {
		cluster.SslOpts = &gocql.SslOptions{
			Config: &tls.Config{
				MinVersion:         tls.VersionTLS12,
				InsecureSkipVerify: true,
			},
		}
	}

	cluster.Authenticator = gocql.PasswordAuthenticator{
		Username: config.Username,
		Password: config.Password,
	}

	cluster.Keyspace = config.Keyspace

	log.Print("cassandra client is connecting")
	client, err := cluster.CreateSession()
	if err != nil {
		log.Printf("failed to connect to cassandra: %v", err)
		return nil, err
	}
	log.Print("cassandra client is connected")
	return client, nil
}

func Disconnect(client *gocql.Session) error {
	log.Print("cassandra client is disconnecting")
	client.Close()
	log.Print("cassandra client is disconnected")
	return nil
}
