package cassandra_database_config

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type CassandraConfig struct {
	ContactPoints []string
	Port          int
	UseSSL        bool
	Username      string
	Password      string
	Keyspace      string
}

func NewCassandraConfig() (*CassandraConfig, error) {
	contactPointsString, existed := os.LookupEnv("CASSANDRA_CONTACT_POINTS")
	if !existed {
		contactPointsString = "127.0.0.1"
	}
	contactPoints := strings.Split(contactPointsString, ",")
	if len(contactPoints) == 0 {
		return nil, fmt.Errorf("empty cassandra contact points")
	}

	portString, existed := os.LookupEnv("CASSANDRA_PORT")
	if !existed {
		portString = "9042"
	}
	port, err := strconv.Atoi(portString)
	if err != nil {
		return nil, fmt.Errorf("invalid cassandra port: %v", err)
	}

	useSSLString, existed := os.LookupEnv("CASSANDRA_USE_SSL")
	if !existed {
		useSSLString = "false"
	}
	useSSL, err := strconv.ParseBool(useSSLString)
	if err != nil {
		return nil, fmt.Errorf("invalid cassandra useSSL: %v", err)
	}

	username, existed := os.LookupEnv("CASSANDRA_USERNAME")
	if !existed {
		username = "cassandra"
	}

	password, existed := os.LookupEnv("CASSANDRA_PASSWORD")
	if !existed {
		password = "cassandra"
	}

	keyspace, existed := os.LookupEnv("CASSANDRA_KEYSPACE")
	if !existed {
		return nil, fmt.Errorf("empty cassandra keyspace")
	}

	return &CassandraConfig{
		ContactPoints: contactPoints,
		Port:          port,
		UseSSL:        useSSL,
		Username:      username,
		Password:      password,
		Keyspace:      keyspace,
	}, nil
}
