package loader

import (
	"log"
	"os"

	"github.com/spf13/viper"
)

const DefaultConfigPath = "./config/local.yaml"

func NewConfigLoader() error {
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		log.Printf("unexpected env var: CONFIG_PATH=EMPTY")
		configPath = DefaultConfigPath
		log.Printf("fallback to env var: CONFIG_PATH=%s", DefaultConfigPath)
	}

	viper.SetConfigFile(configPath)
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	log.Printf("loaded from file config: CONFIG_PATH=%s", configPath)
	return nil
}

