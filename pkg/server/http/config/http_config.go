package http_server_config

import (
	"os"
	"strconv"
)

type HttpConfig struct {
	Port int
}

func NewHttpConfig() (*HttpConfig, error) {
	portString, existed := os.LookupEnv("HTTP_PORT")
	if !existed {
		portString = "8080"
	}
	port, err := strconv.Atoi(portString)
	if err != nil {
		return nil, err
	}

	return &HttpConfig{
		Port: port,
	}, nil
}
