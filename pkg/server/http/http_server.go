package http_server

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	config "gitlab.com/nguyenvhoang99/code-practice-submission-serverless/pkg/server/http/config"
)

func NewHttpServer(config *config.HttpConfig, handler http.Handler) (*http.Server, error) {
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", config.Port),
		Handler: handler,
	}

	listener, err := net.Listen("tcp", server.Addr)
	if err != nil {
		log.Printf("failed to start http listener: %v", err)
		return nil, err
	}

	log.Print("http server is starting")
	go func() {
		if err := server.Serve(listener); err != nil && err != http.ErrServerClosed {
			// Cannot use err to stop due to start in the background, so use fatal to stop
			log.Fatalf("failed to start http server: %v", err)
		}
	}()
	return server, nil
}

func Stop(server *http.Server) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer func() {
		cancel()
	}()
	log.Print("http server is stopping")
	if err := server.Shutdown(ctx); err != nil {
		log.Printf("failed to stop http server: %v", err)
		return err
	}
	log.Print("http server is stopped")
	return nil
}
