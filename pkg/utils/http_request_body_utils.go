package utils

import (
	"bytes"
	"io"
	"net/http"
)

func ClonetHttpRequestBody(req *http.Request) (io.ReadCloser, error) {
	reqBody, err := io.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}

	req.Body.Close()
	req.Body = io.NopCloser(bytes.NewBuffer(reqBody))

	return io.NopCloser(bytes.NewBuffer(reqBody)), nil
}
